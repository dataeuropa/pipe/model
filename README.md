# piveau pipe model

## Table of Contents
1. [Build & Install](#build-and-install)
2. [Use](#use)
3. [License](#license)

## Build and Install
Requirements:
 * Git
 * Maven 3
 * Java 17

```bash
$ git clone <gitrepouri>
$ cd piveau-pipe-model
$ mvn install
```

## Use
Add dependency to your project pom file:
```xml
<dependency>
    <groupId>io.piveau.pipe</groupId>
    <artifactId>pipe-model</artifactId>
    <version>4.3.3</version>
</dependency>
```

## License

[Apache License, Version 2.0](LICENSE.md)
