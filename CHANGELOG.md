# ChangeLog

## Unreleased

## 4.4.1 (2024-06-06)

**Changed:**
* Downgrade jackson lib, because of alignment with implicit transitive dependencies 

## 4.4.0 (2024-05-25)

**Changed:**
* Run id does not need to be a UUID anymore ;-)

## 4.3.11 (2024-04-15)

**Changed:**
* Update lib dependencies

## 4.3.10 (2024-02-14)

**Fixed:**
* Vert.x update ([CVE-2024-1300](https://access.redhat.com/security/cve/CVE-2024-1300))

## 4.3.9 (2023-12-20)

**Changed:**
* Dump lib dependencies

## 4.3.8 (2023-11-14)

Another test release!

## 4.3.7 (2023-11-14)

Failed test release!

## 4.3.6 (2023-11-14)

Another test release!

## 4.3.5 (2023-11-14)

Test release!

## 4.3.4 (2023-11-10)

**Changed:**
* Increased max string length for jackson stream reader even more to 60_000_000

## 4.3.3 (2023-06-01)

**Changed:**
* Increased max string length for jackson stream reader 

## 4.3.2 (2023-025-14)

**Changed:**
* Bump up lib dependencies

## 4.3.1 (2023-02-12)

**Changed:**
* Bump up lib dependencies

## 4.3.0 (2023-02-03)

**Changed:**
* Validation from everis to vertx json schema

## 4.2.3 (2022-11-20)

**Changed:**
* Lib updates

## 4.2.2 (2022-04-19)

**Changed:**
* Lib updates

## 4.2.1 (2022-01-09)

**Changed:**
* Dependencies update

## 4.2.0 (2021-10-09)

**Added:**
* Introduce `keepPayload` pipe property
* `freeData` for better resource management

## 4.1.5 (2021-06-10)

**Changed:**
* Jackson lib update

## 4.1.4 (2021-06-02)

**Changed:**
* Lib dependencies update

## 4.1.3 (2021-02-06)

**Changed:**
* Update dependencies

## 4.1.2 (2020-09-01)

**Changed:**
* Update to Kotlin 1.4.0

## 4.1.0 (2020-07-30)

**Added:**
* Yaml and json specific read methods 

## 4.0.0 (2020-07-13)

**Changed:**
* Package structure for better documentation purposes
 
**Added:**
* Dokka markdown documentation generation
* Reading and writing pipes as yaml

## 3.0.0 (2020-06-11)

**Changed:**
* `PipeManager` API

**Fixed:**
* Use properly configured jacksons object mapper consequently throughout the pipe processing

## 2.1.4 (2020-06-10)

**Fixed:**
* Serialization format of dates to ISO standard

## 2.1.3 (2020-05-27)

Repo relocation

## 2.1.2 (2020-02-28)

Minor fixes

## 2.1.1 (2019-12-10)

**Added:**
* Pretty print as extension for pipe
 
## 2.1.0 (2019-08-28)

**Added:**
* `currentEndpoint`

**Changed:**
* `nextEndpoint` now always after `currentEndpoint`

**Fixed:**
* Typo in `PipeHeader` description

## 2.0.0 (2019-08-21)

**Added:**
* Required property `name` in `PipeHeader`

**Changed:**
* `nextSegment` is first of not processed segments
* `config` not nullable field
* `dataInfo` not nullable field

## 1.0.3 (2019-08-13)

**Fixed:**
* Pipe stringify for deep copy

## 1.0.2 (2019-08-12)

**Fixed:**
* Pipe deep copy
 
## 1.0.1 (2019-08-09)

**Fixed:**
* Data classes now have an empty default constructor

## 1.0.0 (2019-08-09)

**Added:**
* jacoco for code coverage

**Changed:**
* Upgrade to kotlin 1.3.41

**Fixed:**
* Annotate factory methods with JvmStatic

## 0.0.6 (2019-07-30)

**Added:**
* Test stage in gitlab ci

**Changed:**
* Re-implemented everything in kotlin

## 0.0.3-0.0.5
Missed documentation of intermediate releases.

## 0.0.2 (2019-04-15)

**Added:**
- Start time property in pipe model
- ChangeLog #1

**Changed:**
- Tagging version without prefix 'v'

**Fixed:**
- Link to release tag v0.0.1 in ChangeLog

## v0.0.1 (2019-03-01)

Initial release