package io.piveau.pipe

import io.piveau.pipe.model.loadPipe
import io.piveau.pipe.model.prettyPrintJson
import io.piveau.pipe.model.prettyPrintYaml
import io.piveau.pipe.validation.validatePipe
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PipeTest {

    @Test
    fun pipeTest() {
        val pipe = loadPipe("pipe.json")!!
        Assertions.assertTrue(validatePipe(pipe))
    }

    @Test
    fun `Reading json, writing yaml`() {
        val pipe = loadPipe("pipe.json")!!
        println(pipe.prettyPrintYaml())
    }

    @Test
    fun `Reading yaml, writing json`() {
        val pipe = loadPipe("pipe.yaml")!!
        println(pipe.prettyPrintJson())
    }

}
