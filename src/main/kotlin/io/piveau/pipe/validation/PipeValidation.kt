package io.piveau.pipe.validation

import io.piveau.pipe.model.Pipe
import io.piveau.pipe.model.prettyPrintJson
import io.vertx.core.Vertx
import io.vertx.core.json.JsonObject
import io.vertx.json.schema.*

object PipeSchema {
    private val pipeSchema =
        JsonSchema.of(Vertx.vertx().fileSystem().readFileBlocking("piveau-pipe.schema.json").toJsonObject())

    fun validate(pipe: JsonObject): Boolean = Validator
        .create(pipeSchema, JsonSchemaOptions().setDraft(Draft.DRAFT7).setBaseUri("http://io.piveau/"))
        .validate(pipe).valid

}

fun validatePipe(pipe: Pipe): Boolean = PipeSchema.validate(JsonObject(pipe.prettyPrintJson()))
